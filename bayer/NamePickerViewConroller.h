//
//  NamePickerConroller.h
//  bayer
//
//  Created by Ye Myat Min on 10/5/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NamePickerDelegate <NSObject>
@required
- (void)selectedName: (NSDictionary *) name;
@end

@interface NamePickerViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *names;
@property (nonatomic, weak) id<NamePickerDelegate> delegate;
@end
