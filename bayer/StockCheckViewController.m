//
//  StockCheckViewController.m
//  bayer
//
//  Created by Luke on 3/27/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "StockCheckViewController.h"
#import "DAKeyboardControl.h"
#import "UIImageView+WebCache.h"
#import "APIClient.h"
#import "BrandPickerViewController.h"

@interface StockCheckViewController ()
@property (weak, nonatomic) IBOutlet UILabel *leftTitleOutlet;
@property (weak, nonatomic) IBOutlet UILabel *rightTitleOutlet;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (strong, nonatomic) NSArray* data;
@property (strong, nonatomic) NSArray* stock;
@property (strong,nonatomic) BrandPickerViewController *brandPV;
@property (strong,nonatomic) UIPopoverController *brandPO;
@end

@implementation StockCheckViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
   
    
    self.footerOutlet.layer.borderWidth = 1.0f;
    self.footerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
//    [self setMaskTo:self.headerOutlet byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight];
    self.headerOutlet.layer.borderWidth = 1.0f;
    self.headerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    self.stockTable.layer.borderWidth = 1.0f;
    self.stockTable.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    [self.leftTitleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];
    [self.rightTitleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];

    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
    }];
    
    [self loadData];
}

- (void)loadData
{
    [[APIClient sharedInstance] GET:[NSString stringWithFormat: @"%@/%@/%@", @"Service1.svc", @"retrivebrand", [[APIClient sharedInstance].selectedCustomer objectForKey:@"CustomerId"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *myObject = (NSDictionary *) responseObject;
        self.data = [myObject objectForKey:@"LBrands"];
        
        // Hardcoded
        [[APIClient sharedInstance] setSelectedBrand: @"Aleve"];
        self.rightTitleOutlet.text = @"Aleve";
        
        [self loadStock];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Sorry. Please try again later."];
        NSLog(@"Error %@",error);
    }];
}

- (void)loadStock
{
    _stock = nil;
    [self.stockTable reloadData];
    [[APIClient sharedInstance] GET:[NSString stringWithFormat: @"%@/%@/%@/%@", @"Service1.svc", @"CheckStore", [[APIClient sharedInstance].selectedCustomer objectForKey:@"CustomerId"], [APIClient sharedInstance].selectedBrand] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *myArray = (NSArray *) responseObject;
        NSDictionary *myDictionary = [myArray objectAtIndex: 0];
        _stock = [myDictionary objectForKey:@"Products"];
        [self.stockTable reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Sorry. Please try again later."];
        NSLog(@"Error %@",error);
    }];
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_stock count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105.0f;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    UITapGestureRecognizer *tapMe =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker:)];
    ((UILabel *)[cell viewWithTag:101]).userInteractionEnabled = YES;
    [((UILabel *)[cell viewWithTag:101]) addGestureRecognizer:tapMe];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary *data = [_stock objectAtIndex:indexPath.row];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:993];
    UILabel *myLabel = (UILabel *)[cell viewWithTag:999];
    
    myLabel.text = [data objectForKey:@"ItemName"];
    [imageView setImageWithURL:[NSURL URLWithString:[data objectForKey:@"ProductImage"]]
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    return cell;
}

- (void) showDatePicker:(UITapGestureRecognizer *)sender{
    RMDateSelectionViewController *dateSelectionVC = [RMDateSelectionViewController dateSelectionController];
    
    //You can enable or disable bouncing and motion effects
    //dateSelectionVC.disableBouncingWhenShowing = YES;
    //dateSelectionVC.disableMotionEffects = YES;
    
    [dateSelectionVC showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *aDate) {
        NSLog(@"Successfully selected date: %@ (With block)", aDate);
        NSDateFormatter *dateFormatter=[NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd MMM YYYY"];
        ((UILabel *)sender.view).text =[dateFormatter stringFromDate:aDate];
        
    } andCancelHandler:^(RMDateSelectionViewController *vc) {
        NSLog(@"Date selection was canceled (with block)");
    }];
    
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionVC.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionVC.datePicker.minuteInterval = 5;
    dateSelectionVC.datePicker.date = [NSDate dateWithTimeIntervalSinceReferenceDate:0];
    [self.view hideKeyboard];

}


- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)brandClicked:(id)sender
{
    if (_brandPV == nil) {
        _brandPV = [[BrandPickerViewController alloc] initWithStyle: UITableViewStylePlain];
        _brandPV.brands = self.data;
        _brandPV.delegate = self;
    }
    if (_brandPO == nil) {
        _brandPO = [[UIPopoverController alloc] initWithContentViewController:_brandPV];
        [_brandPO presentPopoverFromRect: self.rightTitleOutlet.bounds inView: self.rightTitleOutlet permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_brandPO dismissPopoverAnimated:YES];
        _brandPO = nil;
    }
}

- (void) selectedBrand:(NSString *)brand {
    [[APIClient sharedInstance] setSelectedBrand:brand];
    self.rightTitleOutlet.text = brand;
    [self loadStock];
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    [textField selectAll:self];
}

@end
