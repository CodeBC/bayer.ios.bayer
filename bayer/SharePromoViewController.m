//
//  SharePromoViewController.m
//  bayer
//
//  Created by Luke on 4/3/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "SharePromoViewController.h"
#import "UIImageView+WebCache.h"
#import "APIClient.h"
#import "PBWebViewController.h";

@interface SharePromoViewController ()
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *leftTitleOutlet;
@property (strong, nonatomic) NSArray* data;
@property (strong, nonatomic) NSArray* promos;
@end

@implementation SharePromoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    
    self.footerOutlet.layer.borderWidth = 1.0f;
    self.footerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    //    [self setMaskTo:self.headerOutlet byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight];
    self.headerOutlet.layer.borderWidth = 1.0f;
    self.headerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    self.stockTable.layer.borderWidth = 1.0f;
    self.stockTable.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    [self.leftTitleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];

    [self loadPromos];
}

- (void)loadPromos
{
    [[APIClient sharedInstance] GET:[NSString stringWithFormat: @"%@/%@/%@/%@", @"Service1.svc", @"sharepromotion", [[APIClient sharedInstance].selectedCustomer objectForKey:@"CustomerId"], [[APIClient sharedInstance].selectedOutlet objectForKey: @"Outletid"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *myArray = (NSArray *) responseObject;
        NSDictionary *myDictionary = [myArray objectAtIndex: 0];
        _promos = [myDictionary objectForKey:@"product"];
        [self.stockTable reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Sorry. Please try again later."];
        NSLog(@"Error %@",error);
    }];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_promos count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105.0f;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
//    static NSString *youtubeIdentifier = @"YoutubeCell";
    UITableViewCell *cell;
    
//    if(indexPath.row % 2 == 0) {
//        cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//        if(cell == nil){
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                          reuseIdentifier:MyIdentifier];
//        }
//    }else{
//        cell = [tableView dequeueReusableCellWithIdentifier:youtubeIdentifier];
//        if(cell == nil){
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                          reuseIdentifier:youtubeIdentifier];
//        }
//    }
    
    cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//    UITapGestureRecognizer *tapMe =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker:)];
//    ((UILabel *)[cell viewWithTag:101]).userInteractionEnabled = YES;
//    [((UILabel *)[cell viewWithTag:101]) addGestureRecognizer:tapMe];
//    UITapGestureRecognizer *tapMe2 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker:)];
//    ((UILabel *)[cell viewWithTag:102]).userInteractionEnabled = YES;
//    [((UILabel *)[cell viewWithTag:102]) addGestureRecognizer:tapMe2];
//    
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary* myData = [_promos objectAtIndex:indexPath.row];
    
    UILabel* fromLabel = (UILabel *)[cell viewWithTag:101];
    UILabel* toLabel = (UILabel *)[cell viewWithTag:102];
    UILabel* codeLabel = (UILabel *)[cell viewWithTag:997];
    UILabel* brandLabel = (UILabel *)[cell viewWithTag:103];
    UILabel* itemLabel = (UILabel *)[cell viewWithTag:998];
    UILabel* promoLabel = (UILabel *)[cell viewWithTag:994];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:993];
    
    fromLabel.text = [myData objectForKey:@"PromoStart"];
    toLabel.text = [myData objectForKey:@"PromoEnd"];
    brandLabel.text = [myData objectForKey:@"Brand"];
    codeLabel.text = [myData objectForKey:@"ItemCode"];
    itemLabel.text = [myData objectForKey:@"ItemName"];
    promoLabel.text = [myData objectForKey:@"Promotion"];
    [imageView setImageWithURL:[NSURL URLWithString:[myData objectForKey:@"MarketingActivity"]]
              placeholderImage:[UIImage imageNamed:@"icon_something.png"]];
    
    UITapGestureRecognizer *tapMe3 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openImage:)];
    imageView.userInteractionEnabled = YES;
    [imageView addGestureRecognizer:tapMe3];
    
    return cell;
}

- (void)openImage:(UITapGestureRecognizer *)recognizer
{
    NSLog(@"Tapped");
    CGPoint tapLocation = [recognizer locationInView:self.stockTable];
    NSIndexPath *tapIndexPath = [self.stockTable indexPathForRowAtPoint:tapLocation];
    NSDictionary* myData = [_promos objectAtIndex:tapIndexPath.row];
    
    // Initialize the web view controller and set it's URL
    PBWebViewController* webViewController = [[PBWebViewController alloc] init];
    webViewController.URL = [NSURL URLWithString:[myData objectForKey:@"MarketingActivity"]];
    
    // This property also corresponds to the same one on UIActivityViewController
    // Both properties do not need to be set unless you want custom actions
    webViewController.excludedActivityTypes = @[UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypePostToWeibo];
    
    // Push it
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController: webViewController animated:YES];
}


- (void) showDatePicker:(UITapGestureRecognizer *)sender{
    RMDateSelectionViewController *dateSelectionVC = [RMDateSelectionViewController dateSelectionController];
    
    //You can enable or disable bouncing and motion effects
    //dateSelectionVC.disableBouncingWhenShowing = YES;
    //dateSelectionVC.disableMotionEffects = YES;
    
    [dateSelectionVC showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *aDate) {
        NSLog(@"Successfully selected date: %@ (With block)", aDate);
        NSDateFormatter *dateFormatter=[NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd MMM YYYY"];
        ((UILabel *)sender.view).text =[dateFormatter stringFromDate:aDate];
        
    } andCancelHandler:^(RMDateSelectionViewController *vc) {
        NSLog(@"Date selection was canceled (with block)");
    }];
    
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionVC.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionVC.datePicker.minuteInterval = 5;
    dateSelectionVC.datePicker.date = [NSDate dateWithTimeIntervalSinceReferenceDate:0];
}



- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

@end
