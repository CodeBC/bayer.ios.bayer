//
//  StockCheckViewController.h
//  bayer
//
//  Created by Luke on 3/27/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMDateSelectionViewController.h"
#import "BrandPickerViewController.h"

@interface StockCheckViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RMDateSelectionViewControllerDelegate, BrandPickerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *stockTable;
@property (weak, nonatomic) IBOutlet UIView *headerOutlet;
@property (weak, nonatomic) IBOutlet UIView *footerOutlet;

- (IBAction)brandClicked:(id)sender;

@end
