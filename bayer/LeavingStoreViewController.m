//
//  LeavingStoreViewController.m
//  bayer
//
//  Created by Luke on 4/11/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "LeavingStoreViewController.h"
#import "SVProgressHUD.h"

@interface LeavingStoreViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerOutlet;
@property (weak, nonatomic) IBOutlet UILabel *titleOutlet;
@property (weak, nonatomic) IBOutlet UILabel *timeoutOutlet;
@property (weak, nonatomic) IBOutlet UIButton *firstCheck;
@property (weak, nonatomic) IBOutlet UIButton *secondCheck;

@end

@implementation LeavingStoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.containerOutlet.layer.borderColor = [UIColor colorWithRed:163/255.0 green:162/255.0 blue:162/255.0 alpha:1].CGColor;
    [self.titleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];

    // Convert date time stuff
    NSDateFormatter *formatter;
    NSString        *dateString;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy HH:mm"];
    dateString = [formatter stringFromDate:[NSDate date]];
    self.timeoutOutlet.text = dateString;
}

- (IBAction)checkBtn:(UIButton *)sender {
    [sender setSelected:!sender.selected];
}
- (IBAction)backToRoot:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (IBAction)save:(id)sender {
    if (self.firstCheck.selected == YES && self.secondCheck.selected == YES) {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Are you sure?"
                                                         message:@"Do you want to save the data?"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles: nil];
        [alert addButtonWithTitle:@"Yes"];
        [alert show];
    } else {
        [SVProgressHUD showErrorWithStatus:@"Please check first"];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
