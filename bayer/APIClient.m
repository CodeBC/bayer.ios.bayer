//
//  APIClient.m
//  bayer
//
//  Created by Ye Myat Min on 10/5/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "APIClient.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"

@implementation APIClient

@synthesize selectedBrand, selectedCustomer, selectedOutlet, selectedName;

+ (instancetype) sharedInstance {
    static APIClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[APIClient alloc] initWithBaseURL:
                            [NSURL URLWithString: API_BASE]];
    });
    
    return __sharedInstance;
}

@end
