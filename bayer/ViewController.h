//
//  ViewController.h
//  bayer
//
//  Created by Luke on 3/26/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *titleOutlet;
@property (weak, nonatomic) IBOutlet UILabel *subtitleOutlet;

- (IBAction)syncClicked:(id)sender;

@end
