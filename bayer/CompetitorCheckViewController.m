//
//  CompetitorCheckViewController.m
//  bayer
//
//  Created by Luke on 4/3/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "CompetitorCheckViewController.h"
#import "DAKeyboardControl.h"
#import "BrandPickerViewController.h"
#import "APIClient.h"

@interface CompetitorCheckViewController ()
@property (weak, nonatomic) IBOutlet UILabel *leftTitleOutlet;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (strong, nonatomic) NSMutableArray* brandArr;
@property (strong, nonatomic) NSMutableArray* itemnameArr;
@property (strong, nonatomic) NSMutableArray* promotionArr;

@property (strong, nonatomic) UIButton* curButton;

@property (strong,nonatomic) BrandPickerViewController *brandPV;
@property (strong,nonatomic) UIPopoverController *brandPO;

@property (strong,nonatomic) BrandPickerViewController *itemnamePV;
@property (strong,nonatomic) UIPopoverController *itemnamePO;

@property (strong,nonatomic) BrandPickerViewController *promotionPV;
@property (strong,nonatomic) UIPopoverController *promotionPO;


@end

@implementation CompetitorCheckViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    
    self.footerOutlet.layer.borderWidth = 1.0f;
    self.footerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    //    [self setMaskTo:self.headerOutlet byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight];
    self.headerOutlet.layer.borderWidth = 1.0f;
    self.headerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    self.stockTable.layer.borderWidth = 1.0f;
    self.stockTable.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    [self.leftTitleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];
    
    CGRect oldFrame = self.stockTable.frame;
    
    
//    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
//        if(opening){
//            CGRect tableViewFrame = self.stockTable.frame;
//            tableViewFrame.origin.y = keyboardFrameInView.origin.y - tableViewFrame.size.height;
//            self.stockTable.frame = tableViewFrame;
//        }else{
//            self.stockTable.frame = oldFrame;
//        }
//        
//        
//    }];
    [self loadData];
}

- (void)loadData
{
    [[APIClient sharedInstance] GET:[NSString stringWithFormat: @"%@/%@", @"Service1.svc", @"Checkcompetitor"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *myArray = (NSArray *) responseObject;
        NSDictionary* data = [myArray objectAtIndex: 0];
        
        self.brandArr = [[NSMutableArray alloc] init];
        self.itemnameArr = [[NSMutableArray alloc] init];
        self.promotionArr = [[NSMutableArray alloc] init];
        
        NSArray *tempBrandArr = [data objectForKey: @"brand"];
        for (NSDictionary* dict in tempBrandArr) {
            [self.brandArr addObject:[dict objectForKey:@"brands"]];
        }
        
        NSArray *tempItemnameArr = [data objectForKey: @"itemname"];
        for (NSDictionary* dict in tempItemnameArr) {
            [self.itemnameArr addObject:[dict objectForKey:@"itemname"]];
        }
        
        NSArray *tempPromotionArr = [data objectForKey: @"promotion"];
        for (NSDictionary* dict in tempPromotionArr) {
            [self.promotionArr addObject:[dict objectForKey:@"prom"]];
        }
        
        [self.stockTable reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Sorry. Please try again later."];
        NSLog(@"Error %@",error);
    }];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130.0f;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    UITapGestureRecognizer *tapMe =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker:)];
    ((UILabel *)[cell viewWithTag:101]).userInteractionEnabled = YES;
    [((UILabel *)[cell viewWithTag:101]) addGestureRecognizer:tapMe];
    UITapGestureRecognizer *tapMe2 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker:)];
    ((UILabel *)[cell viewWithTag:102]).userInteractionEnabled = YES;
    [((UILabel *)[cell viewWithTag:102]) addGestureRecognizer:tapMe2];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }
    
    UITapGestureRecognizer *tapMe3 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showBrandPicker:)];
    ((UIButton *)[cell viewWithTag:123]).userInteractionEnabled = YES;
    [((UIButton *)[cell viewWithTag:123]) addGestureRecognizer:tapMe3];
    
    UITapGestureRecognizer *tapMe4 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showItemPicker:)];
    ((UIButton *)[cell viewWithTag:124]).userInteractionEnabled = YES;
    [((UIButton *)[cell viewWithTag:124]) addGestureRecognizer:tapMe4];

    UITapGestureRecognizer *tapMe5 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPromoPicker:)];
    ((UIButton *)[cell viewWithTag:125]).userInteractionEnabled = YES;
    [((UIButton *)[cell viewWithTag:125]) addGestureRecognizer:tapMe5];
    
    UITapGestureRecognizer *tapMe6 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showCamera:)];
    ((UIView *)[cell viewWithTag:104]).userInteractionEnabled = YES;
    [((UIView *)[cell viewWithTag:104]) addGestureRecognizer:tapMe6];
    
    
    // Here we use the provided setImageWithURL: method to load the web image
    // Ensure you use a placeholder image otherwise cells will be initialized with no image
    
    return cell;
}

- (void)showBrandPicker:(UIGestureRecognizer *) recognizer
{
    self.curButton = (UIButton *)recognizer.view;
    if (_brandPV == nil) {
        _brandPV = [[BrandPickerViewController alloc] initWithStyle: UITableViewStylePlain];
        _brandPV.brands = self.brandArr;
        _brandPV.delegate = self;
    }
    if (_brandPO == nil) {
        _brandPO = [[UIPopoverController alloc] initWithContentViewController:_brandPV];
        [_brandPO presentPopoverFromRect: recognizer.view.bounds inView: recognizer.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_brandPO dismissPopoverAnimated:YES];
        _brandPO = nil;
    }
}

- (void)showItemPicker:(UIGestureRecognizer *) recognizer
{
    self.curButton = (UIButton *)recognizer.view;
    if (_itemnamePV == nil) {
        _itemnamePV = [[BrandPickerViewController alloc] initWithStyle: UITableViewStylePlain];
        _itemnamePV.brands = self.itemnameArr;
        _itemnamePV.delegate = self;
    }
    if (_itemnamePO == nil) {
        _itemnamePO = [[UIPopoverController alloc] initWithContentViewController:_itemnamePV];
        [_itemnamePO presentPopoverFromRect: recognizer.view.bounds inView: recognizer.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_itemnamePO dismissPopoverAnimated:YES];
        _itemnamePO = nil;
    }
}

- (void)showPromoPicker:(UIGestureRecognizer *) recognizer
{
    self.curButton = (UIButton *)recognizer.view;
    if (_promotionPV == nil) {
        _promotionPV = [[BrandPickerViewController alloc] initWithStyle: UITableViewStylePlain];
        _promotionPV.brands = self.promotionArr;
        _promotionPV.delegate = self;
    }
    if (_promotionPO == nil) {
        _promotionPO = [[UIPopoverController alloc] initWithContentViewController:_promotionPV];
        [_promotionPO presentPopoverFromRect: recognizer.view.bounds inView: recognizer.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_promotionPO dismissPopoverAnimated:YES];
        _promotionPO = nil;
    }
}

- (void)showCamera:(UIGestureRecognizer *) recognizer
{
    UIImagePickerController *imagePicker =
    [[UIImagePickerController alloc] init];
    
    imagePicker.delegate = self;
    
   imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    imagePicker.mediaTypes =
    @[(NSString *) kUTTypeImage];
    
    imagePicker.allowsEditing = YES;
    [self presentViewController:imagePicker
                       animated:YES completion:nil];
}

-(void)imagePickerController:
(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Code here to work with media
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:
(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) showDatePicker:(UITapGestureRecognizer *)sender{
    RMDateSelectionViewController *dateSelectionVC = [RMDateSelectionViewController dateSelectionController];
    
    //You can enable or disable bouncing and motion effects
    //dateSelectionVC.disableBouncingWhenShowing = YES;
    //dateSelectionVC.disableMotionEffects = YES;
    
    [dateSelectionVC showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *aDate) {
        NSLog(@"Successfully selected date: %@ (With block)", aDate);
        NSDateFormatter *dateFormatter=[NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd MMM YYYY"];
        ((UILabel *)sender.view).text =[dateFormatter stringFromDate:aDate];
        
    } andCancelHandler:^(RMDateSelectionViewController *vc) {
        NSLog(@"Date selection was canceled (with block)");
    }];
    
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionVC.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionVC.datePicker.minuteInterval = 5;
    dateSelectionVC.datePicker.date = [NSDate dateWithTimeIntervalSinceReferenceDate:0];
}

- (void)selectedBrand: (NSString *) brand {
    self.curButton.titleLabel.text = brand;
    NSLog(@"%@", self.curButton.titleLabel.text);

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
