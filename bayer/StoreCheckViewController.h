//
//  StoreCheckViewController.h
//  bayer
//
//  Created by Luke on 3/26/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerPickerViewController.h"
#import "OutletPickerViewController.h"
#import "NamePickerViewConroller.h"


@interface StoreCheckViewController : UIViewController <UIPickerViewDelegate, CustomerPickerDelegate, OutletPickerDelegate, NamePickerDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerOutlet;

- (IBAction)customerClicked:(id)sender;
- (IBAction)outletClicked:(id)sender;
- (IBAction)nameClicked:(id)sender;

@end
