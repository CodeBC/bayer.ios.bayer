//
//  NamePickerConroller.h
//  bayer
//
//  Created by Ye Myat Min on 10/5/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BrandPickerDelegate <NSObject>
@required
- (void)selectedBrand: (NSString *) brand;
@end

@interface BrandPickerViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *brands;
@property (nonatomic, weak) id<BrandPickerDelegate> delegate;
@end
