//
//  StoreVisitedViewController.m
//  bayer
//
//  Created by Luke on 4/11/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "StoreVisitedViewController.h"

@interface StoreVisitedViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleOutlet;
@property (weak, nonatomic) IBOutlet UIView *containerOutlet;

@end

@implementation StoreVisitedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.containerOutlet.layer.borderColor = [UIColor colorWithRed:163/255.0 green:162/255.0 blue:162/255.0 alpha:1].CGColor;
    [self.titleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];
    // Do any additional setup after loading the view.
}

- (IBAction)backToRoot:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
