//
//  Constants.h
//  bayer
//
//  Created by Ye Myat Min on 10/5/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject


extern NSString * const API_BASE;

@end
