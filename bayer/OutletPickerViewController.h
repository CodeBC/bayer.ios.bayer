//
//  NamePickerConroller.h
//  bayer
//
//  Created by Ye Myat Min on 10/5/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OutletPickerDelegate <NSObject>
@required
- (void)selectedOutlet: (NSDictionary *) outlet;
@end

@interface OutletPickerViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *outlets;
@property (nonatomic, weak) id<OutletPickerDelegate> delegate;
@end
