//
//  SecondStockCheckViewController.m
//  bayer
//
//  Created by Luke on 4/3/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "SecondStockCheckViewController.h"
#import "PBWebViewController.h"
#import "CompetitorCheckViewController.h"
#import "APIClient.h"

@interface SecondStockCheckViewController ()
@property (weak, nonatomic) IBOutlet UILabel *leftTitleOutlet;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *rightTitleOutlet;
@property (strong, nonatomic) NSArray* data;
@property (strong, nonatomic) NSArray* promos;
@end

@implementation SecondStockCheckViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    
    self.footerOutlet.layer.borderWidth = 1.0f;
    self.footerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    //    [self setMaskTo:self.headerOutlet byRoundingCorners:UIRectCornerTopLeft|UIRectCornerTopRight];
    self.headerOutlet.layer.borderWidth = 1.0f;
    self.headerOutlet.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    self.stockTable.layer.borderWidth = 1.0f;
    self.stockTable.layer.borderColor =[UIColor colorWithRed:134/255.0 green:134/255.0 blue:134/255.0 alpha:1].CGColor;
    
    [self.leftTitleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];
    [self.rightTitleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];
    [self loadPromos];
}


- (void)loadPromos
{
    NSLog(@"Loading");
    [[APIClient sharedInstance] GET:[NSString stringWithFormat: @"%@/%@/%@/%@", @"Service1.svc", @"Checkpromotion", [[APIClient sharedInstance].selectedCustomer objectForKey:@"CustomerId"], [[APIClient sharedInstance].selectedOutlet objectForKey: @"Outletid"]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *myArray = (NSArray *) responseObject;
        NSDictionary *myDictionary = [myArray objectAtIndex: 0];
        NSArray *test = [myDictionary objectForKey:@"Productsp"];
        NSMutableArray *sample = [[NSMutableArray alloc]init];
        for(NSDictionary * dict in test) {
            NSMutableDictionary *new = [[NSMutableDictionary alloc] initWithDictionary:dict];
            [new setObject:[NSNumber numberWithBool:NO] forKey:@"is_checked"];
            [sample addObject:new];
        }
        _promos = [NSArray arrayWithArray:sample];
        NSLog(@"Done");
        [self.stockTable reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Sorry. Please try again later."];
        NSLog(@"Error %@",error);
    }];
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIButton* checkBox = (UIButton *)[cell viewWithTag:100];
    
    BOOL status = checkBox.isSelected;
    checkBox.selected = !status;
    
    NSDictionary* myData = [_promos objectAtIndex:indexPath.row];
    [myData setValue:[NSNumber numberWithBool:checkBox.isSelected] forKey:@"is_checked"];

}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_promos count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105.0f;
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//    [((UIButton *) [cell viewWithTag:100]) addTarget:self action:@selector(tapMe:) forControlEvents:UIControlEventTouchDown];
    
// Period shouldn't be editable (8 May)
    
//    UITapGestureRecognizer *tapMe =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker:)];
//    ((UILabel *)[cell viewWithTag:101]).userInteractionEnabled = YES;
//    [((UILabel *)[cell viewWithTag:101]) addGestureRecognizer:tapMe];
//    UITapGestureRecognizer *tapMe2 =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDatePicker:)];
//    ((UILabel *)[cell viewWithTag:102]).userInteractionEnabled = YES;
//    [((UILabel *)[cell viewWithTag:102]) addGestureRecognizer:tapMe2];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:MyIdentifier];
    }

    if (indexPath.row != 0) {
        UIImageView* myImage = (UIImageView *)[cell viewWithTag:107];
        [myImage removeFromSuperview];
    }
    
    NSDictionary* myData = [_promos objectAtIndex:indexPath.row];
    
    UILabel* fromLabel = (UILabel *)[cell viewWithTag:101];
    UILabel* toLabel = (UILabel *)[cell viewWithTag:102];
    UILabel* brandLabel = (UILabel *)[cell viewWithTag:103];
    UILabel* itemLabel = (UILabel *)[cell viewWithTag:998];
    UILabel* promoLabel = (UILabel *)[cell viewWithTag:997];
    UILabel* posmLabel = (UILabel *)[cell viewWithTag:104];
    UILabel* secondaryLabel = (UILabel *)[cell viewWithTag:105];
    UIButton* checkBox = (UIButton *)[cell viewWithTag:100];
    
    fromLabel.text = [myData objectForKey:@"PromoStart"];
    toLabel.text = [myData objectForKey:@"PromoEnd"];
    brandLabel.text = [myData objectForKey:@"Brand"];
    itemLabel.text = [myData objectForKey:@"ItemName"];
    promoLabel.text = [myData objectForKey:@"Promotion"];
    posmLabel.text = [myData objectForKey:@"POSM"];
    secondaryLabel.text = [myData objectForKey:@"SecDispType"];
    
    if ([[myData objectForKey:@"is_checked"] isEqualToNumber:[NSNumber numberWithBool:YES]]) {
        checkBox.selected = YES;
    } else {
        checkBox.selected = NO;
    }
    
    return cell;
}


- (void) openPdf:(UITapGestureRecognizer *) sender {
    NSLog(@"Tapped");
    // Initialize the web view controller and set it's URL
    PBWebViewController* webViewController = [[PBWebViewController alloc] init];
    webViewController.URL = [NSURL URLWithString:@"http://iwh.com.sg/bayer/LOA.pdf"];
    
    // This property also corresponds to the same one on UIActivityViewController
    // Both properties do not need to be set unless you want custom actions
    webViewController.excludedActivityTypes = @[UIActivityTypeMail, UIActivityTypeMessage, UIActivityTypePostToWeibo];
    
    // Push it
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController: webViewController animated:YES];
}


- (void) showDatePicker:(UITapGestureRecognizer *)sender{
    RMDateSelectionViewController *dateSelectionVC = [RMDateSelectionViewController dateSelectionController];
    
    //You can enable or disable bouncing and motion effects
    //dateSelectionVC.disableBouncingWhenShowing = YES;
    //dateSelectionVC.disableMotionEffects = YES;
    
    [dateSelectionVC showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *aDate) {
        NSLog(@"Successfully selected date: %@ (With block)", aDate);
        NSDateFormatter *dateFormatter=[NSDateFormatter new];
        [dateFormatter setDateFormat:@"dd MMM YYYY"];
        ((UILabel *)sender.view).text =[dateFormatter stringFromDate:aDate];
        
    } andCancelHandler:^(RMDateSelectionViewController *vc) {
        NSLog(@"Date selection was canceled (with block)");
    }];
    
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionVC.datePicker.datePickerMode = UIDatePickerModeDate;
    dateSelectionVC.datePicker.minuteInterval = 5;
    dateSelectionVC.datePicker.date = [NSDate dateWithTimeIntervalSinceReferenceDate:0];
}


- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    Boolean canPass = true;
    if ([_promos count] > 0) {
        for(NSDictionary *dict in _promos) {
            if ([[dict valueForKey:@"is_checked"] isEqualToNumber:[NSNumber numberWithBool:NO]]) {
                canPass = false;
                break;
            }
        }
        
        NSLog(@"Can pass %d", canPass);
        
        if(canPass == false) {
            return NO;
        }
    }
    return canPass;
}


@end
