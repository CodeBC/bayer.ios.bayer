//
//  LeavingStoreViewController.h
//  bayer
//
//  Created by Luke on 4/11/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeavingStoreViewController : UIViewController<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
- (IBAction)save:(id)sender;
@end
