//
//  APIClient.h
//  bayer
//
//  Created by Ye Myat Min on 10/5/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface APIClient : AFHTTPRequestOperationManager

+ (instancetype)sharedInstance;

@property (retain, nonatomic) NSDictionary* selectedCustomer;
@property (retain, nonatomic) NSDictionary* selectedOutlet;
@property (retain, nonatomic) NSDictionary* selectedName;
@property (retain, nonatomic) NSString* selectedBrand;

@end
