//
//  SharePromoViewController.h
//  bayer
//
//  Created by Luke on 4/3/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMDateSelectionViewController.h"

@interface SharePromoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RMDateSelectionViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *stockTable;
@property (weak, nonatomic) IBOutlet UIView *headerOutlet;
@property (weak, nonatomic) IBOutlet UIView *footerOutlet;

@end
