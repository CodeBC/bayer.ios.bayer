//
//  CustomerPickerViewController.h
//  bayer
//
//  Created by Ye Myat Min on 10/5/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomerPickerDelegate <NSObject>
@required
- (void)selectedCustomer: (NSDictionary *) customer;
@end

@interface CustomerPickerViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray *customers;
@property (nonatomic, weak) id<CustomerPickerDelegate> delegate;
@end
