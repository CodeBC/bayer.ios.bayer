//
//  ViewController.m
//  bayer
//
//  Created by Luke on 3/26/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.titleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];
    
    [self.subtitleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)syncClicked:(id)sender
{
    [SVProgressHUD showSuccessWithStatus:@"Successfully synchronised"];
}

@end
