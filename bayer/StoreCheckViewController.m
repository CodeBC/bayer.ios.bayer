//
//  StoreCheckViewController.m
//  bayer
//
//  Created by Luke on 3/26/14.
//  Copyright (c) 2014 Luke. All rights reserved.
//

#import "StoreCheckViewController.h"
#import "DAKeyboardControl.h"
#import "APIClient.h"

@interface StoreCheckViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleOutlet;
@property (weak, nonatomic) IBOutlet UITextField *staffNameTF;
@property (weak, nonatomic) IBOutlet UILabel *welcomeTF;
@property (weak, nonatomic) IBOutlet UIButton *customerButton;
@property (weak, nonatomic) IBOutlet UIButton *outletButton;
@property (weak, nonatomic) IBOutlet UIButton *nameButton;
@property (weak, nonatomic) IBOutlet UILabel *dateTF;
@property (weak, nonatomic) IBOutlet UILabel *timeTF;

@property (strong, nonatomic) NSDictionary* data;
@property (strong, nonatomic) NSMutableArray* customerArr;
@property (strong, nonatomic) NSMutableArray* outletArr;
@property (strong, nonatomic) NSMutableArray* nameArr;
@property (strong, nonatomic) NSString* welcomeMessage;

@property (strong,nonatomic) CustomerPickerViewController *customerPV;
@property (strong,nonatomic) UIPopoverController *customerPO;

@property (strong,nonatomic) OutletPickerViewController *outletPV;
@property (strong,nonatomic) UIPopoverController *outletPO;

@property (strong,nonatomic) NamePickerViewController *namePV;
@property (strong,nonatomic) UIPopoverController *namePO;

@end

@implementation StoreCheckViewController

- (IBAction)goBack:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.containerOutlet.layer.borderColor = [UIColor colorWithRed:163/255.0 green:162/255.0 blue:162/255.0 alpha:1].CGColor;
    [self.titleOutlet setFont:[UIFont fontWithName:@"Blanch-Caps" size:70]];

    self.staffNameTF.layer.cornerRadius = 2;
    self.staffNameTF.layer.borderWidth = 1.0f;
    self.staffNameTF.layer.borderColor =[UIColor colorWithRed:206/255.0 green:205/255.0 blue:205/255.0 alpha:1].CGColor;
    
    self.staffNameTF.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0);
    
	// Do any additional setup after loading the view.
    
    self.view.keyboardTriggerOffset = 40;
    
    // Convert date time stuff
    NSDateFormatter *formatter;
    NSString        *dateString;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMM yyyy"];
    dateString = [formatter stringFromDate:[NSDate date]];
    self.dateTF.text = dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    dateString = [formatter stringFromDate:[NSDate date]];
    self.timeTF.text = dateString;
    
    [self loadData];
    
}

- (void) viewDidAppear:(BOOL)animated {
    
    [self.view addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        CGRect tableViewFrame = self.view.frame;
        tableViewFrame.size.height = keyboardFrameInView.origin.y;
        self.view.frame = tableViewFrame;
    }];
}

- (void) viewDidDisappear:(BOOL)animated{
    [self.view removeKeyboardControl];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData
{
    [[APIClient sharedInstance] GET:[NSString stringWithFormat: @"%@/%@", @"Service1.svc", @"Storecheck"] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *myArray = (NSArray *) responseObject;
        self.data = [myArray objectAtIndex: 0];
        
        self.customerArr = [self.data objectForKey: @"Cutomer"];
        self.outletArr = [self.data objectForKey: @"Outlet"];
        self.nameArr = [self.data objectForKey: @"name"];
        self.welcomeMessage = [self.data objectForKey:@"welcomeMessage"];
        
        
        self.welcomeTF.text = self.welcomeMessage;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD showErrorWithStatus:@"Sorry. Please try again later."];
        NSLog(@"Error %@",error);
    }];
}

- (IBAction)customerClicked:(id)sender
{
    if (_customerPV == nil) {
        _customerPV = [[CustomerPickerViewController alloc] initWithStyle: UITableViewStylePlain];
        _customerPV.customers = self.customerArr;
        _customerPV.delegate = self;
    }
    if (_customerPO == nil) {
        _customerPO = [[UIPopoverController alloc] initWithContentViewController:_customerPV];
        [_customerPO presentPopoverFromRect: self.customerButton.bounds inView:self.customerButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_customerPO dismissPopoverAnimated:YES];
        _customerPO = nil;
    }
}

- (void)selectedCustomer:(NSDictionary *)customer
{
    [[APIClient sharedInstance] setSelectedCustomer: customer];
    self.customerButton.titleLabel.text = [customer objectForKey:@"customer"];
    [_customerPO dismissPopoverAnimated:YES];
    _customerPO = nil;
}

- (IBAction)outletClicked:(id)sender
{
    if (_outletPV == nil) {
        _outletPV = [[OutletPickerViewController alloc] initWithStyle: UITableViewStylePlain];
        _outletPV.outlets = self.outletArr;
        _outletPV.delegate = self;
        NSLog(@"%@", self.outletArr);
    }
    if (_outletPO == nil) {
        _outletPO = [[UIPopoverController alloc] initWithContentViewController:_outletPV];
        [_outletPO presentPopoverFromRect: self.outletButton.bounds inView:self.outletButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_outletPO dismissPopoverAnimated:YES];
        _outletPO = nil;
    }
}

- (void)selectedOutlet:(NSDictionary *)outlet
{
    [[APIClient sharedInstance] setSelectedOutlet: outlet];
    self.outletButton.titleLabel.text = [outlet objectForKey:@"outletCode"];
    [_outletPO dismissPopoverAnimated:YES];
    _outletPO = nil;
}

- (IBAction)nameClicked:(id)sender
{
    if (_namePV == nil) {
        _namePV = [[NamePickerViewController alloc] initWithStyle: UITableViewStylePlain];
        _namePV.names = self.nameArr;
        _namePV.delegate = self;
    }
    if (_namePO == nil) {
        _namePO = [[UIPopoverController alloc] initWithContentViewController:_namePV];
        [_namePO presentPopoverFromRect: self.nameButton.bounds inView:self.nameButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    } else {
        [_namePO dismissPopoverAnimated:YES];
        _namePO = nil;
    }
}

- (void)selectedName:(NSDictionary *)name
{
    [[APIClient sharedInstance] setSelectedBrand: name];
    self.nameButton.titleLabel.text = [name objectForKey:@"EmpName"];
    [_namePO dismissPopoverAnimated:YES];
    _namePO = nil;
}

@end
